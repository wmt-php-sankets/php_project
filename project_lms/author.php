<?php require "logout.php"; ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <title></title>
</head>

<body>
  <div class="container-fluid">
    <?php require "nav.php"; ?>
    <?php
    require "connection.php";
    $data = $conn->query(("SELECT * FROM author_"));

    ?>
    <div class="text-center mt-5">
      <button class="btn btn-primary"> <a href="form1.php" class="text-white">Add Author Data</a> </button>
    </div>

    <div class="row justify-content-center mt-5 mx-0 px-0">
      <table class="table">
        <thead>
          <tr>

            <th>Full_Name</th>

            <th>DOB</th>
            <th>Gender</th>
            <th>Address</th>
            <th>Mobile_No</th>
            <th>Description</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <?php
        while ($row = $data->fetch_assoc()) {

        ?>
          <tr>
            <td><?php echo $row['first_name']; ?></td>

            <td><?php echo $row['dob']; ?></td>
            <td><?php echo $row['gender']; ?></td>
            <td><?php echo $row['address_']; ?></td>
            <td><?php echo $row['mobile_no']; ?></td>
            <td><?php echo $row['description_']; ?></td>
            <td><?php echo $row['status_']; ?></td>
            <td>
              <a href="update.php?id=<?php echo $row['id'] ?>" class="btn btn-info">UPDATE</a>
              <a href="delete.php?delete=<?php echo $row['id']; ?>  " class="btn btn-danger">DELETE</a>
            </td>

          </tr>

        <?php
        }
        ?>
      </table>
    </div>

  </div>
</body>

</html>