<!DOCTYPE html>
<html lang="en">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <title>Show</title>
</head>

<body>
  <?php require "nav.php"; ?>

  <div class="container-fluid">
    <div class="text-center mt-5">
      <h1>Show Details</h1>
    </div>
  </div>


  <div class="container-fluid">
    <?php
    require "connection.php";
    $data = $conn->query(("SELECT * FROM book_"));

    ?>

    <div class="row justify-content-center mt-5 mx-0 px-0">
      <table class="table">
        <thead>
          <tr>
            <th>Title</th>
            <th>Pages</th>
            <th>Langauge</th>
            <th>Book_author</th>
            <th>Cover_Image</th>
            <th>Isbn</th>
            <th>Description</th>
          </tr>
        </thead>

        <?php
        require "connection.php";
        if (isset($_GET['show'])) {
          $id = $_GET['show'];
          $qry = "select * from book_ where book_id='$id'";
          $result = $conn->query($qry);

          if ($result->num_rows > 0) {
            while ($rows = $result->fetch_assoc()) {
              $title = $rows['title'];
              $pages = $rows['pages'];
              $langauge = $rows['langauge'];
              $book_author = $rows['book_author'];
              $cover_image = $rows['cover_image'];
              $isbn = $rows['isbn'];
              $description = $rows['description'];
            }
          }
        }

        ?>

        <tr>
          <td><?php echo $title; ?></td>
          <td><?php echo $pages; ?></td>
          <td><?php echo $langauge; ?></td>
          <td><?php echo $book_author; ?></td>
          <td><?php echo $cover_image; ?></td>
          <td><?php echo $isbn; ?></td>
          <td><?php echo $description; ?></td>
        </tr>

      </table>
    </div>

  </div>
</body>

</html>