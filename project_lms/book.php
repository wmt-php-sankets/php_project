<?php require "logout.php"; ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <title>Document</title>
</head>

<body>
  <?php require "nav.php"; ?>

  <div class="container-fluid">
    <div class="text-center mt-5">
      <button class="btn btn-primary"> <a href="book_details.php" class="text-white">ADD BooK DATA</a> </button>
    </div>
  </div>


  <div class="container-fluid">
    <?php
    require "connection.php";
    $data = $conn->query(("SELECT * FROM book_"));

    ?>

    <div class="row justify-content-center mt-5 mx-0 px-0">
      <table class="table">
        <thead>
          <tr>
            <th>Title</th>
            <th>Pages</th>
            <th>Langauge</th>
            <th>Book_author</th>
            <th>Cover_Image</th>
            <th>Isbn</th>
            <th>Description</th>
            <th>Status</th>
            <th>Action</th>
            <th>Status</th>
          </tr>
        </thead>
        <?php
        while ($row = $data->fetch_assoc()) {

        ?>
          <tr>
            <td><?php echo $row['title']; ?></td>
            <td><?php echo $row['pages']; ?></td>
            <td><?php echo $row['langauge']; ?></td>
            <td><?php echo $row['book_author']; ?></td>
            <td><?php echo $row['cover_image']; ?></td>
            <td><?php echo $row['isbn']; ?></td>
            <td><?php echo $row['description']; ?></td>
            <td><?php echo $row['status']; ?></td>
            <td>
              <a href="update_book.php?id=<?php echo $row['book_id'] ?>" class="btn btn-info">UPDATE</a>
              <a href="delete_book.php?delete=<?php echo $row['book_id']; ?>  " class="btn btn-danger">DELETE</a>
              <a href="show.php?show=<?php echo $row['book_id']; ?>  " class="btn btn-info">Show</a>
            </td>
            <td>
              <?php
              if ($row['status'] == 1) {
                echo "<td><a href='active.php?active=$row[book_id]' class='btn btn-danger'>Disable</td>";
              } else {
                echo "<td><a href='active.php?active=$row[book_id]' class='btn btn-success'>Active</td>";
              }
              ?>
            </td>

          </tr>

        <?php
        }
        ?>
      </table>
    </div>

  </div>
</body>

</html>